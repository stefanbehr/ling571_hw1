# Stefan Behr
# LING 571
# Homework 1
# 1/18/2013

Executable = /opt/python-2.7/bin/python2.7
Universe   = vanilla
getenv     = true
input      = 
output     = hw1.out
error      = hw1.error
Log        = hw1.log
arguments  = hw1.py grammar.cfg sentences.txt
transfer_executable = false
request_memory = 2*1024
Queue