#!/usr/bin/env python2.7

# Stefan Behr
# LING 571
# Homework 1
# tokenization, parsing, reporting code

import nltk, sys

try:
    grammar_path = sys.argv[1]
except IndexError:
    exit("Please provide a pathname argument pointing to a grammar file.")

try:
    inpath = sys.argv[2]
except IndexError:
    exit("Please provide a pathname argument pointing to an input file of test sentences.")

grammar = nltk.data.load("file:{0}".format(grammar_path))
parser = nltk.parse.EarleyChartParser(grammar)

with open(inpath) as infile:
    sentences = infile.read().strip().split("\n")

# sentence cutoff for grammar testing purposes
try:
    firstn = int(sys.argv[3])
except IndexError:
    firstn = len(sentences)

# statistics variables
parse_total = 0
sentence_total = firstn
unparsed = 0

print """# Stefan Behr
# LING 571
# Homework 1
# output
"""

# iterate through sentences, tokenizing and parsing each one
# pretty print each parse for each sentence
for i, sent in enumerate(sentences[:firstn], start=1):
    print "[Sentence {0}: \"{1}\"]".format(i, sent)
    tokens = nltk.wordpunct_tokenize(sent)
    parses = parser.nbest_parse(tokens)
    print "{0} parses found".format(len(parses))
    if len(parses) == 0:
        unparsed += 1
    parse_total += len(parses)
    for j, tree in enumerate(parses, start=1):
        print "[Parse {0}]".format(j)
        print tree.pprint()
    print

# reporting statements
print "Average parses per sentence: {0:.4f}".format(float(parse_total)/sentence_total)
print "Number of unparsed sentences: {0}".format(unparsed)
