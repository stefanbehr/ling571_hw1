#!/usr/bin/env python

import sys

try:
    inpath = sys.argv[1]
except IndexError:
    exit("Please provide an input file.")

with open(inpath) as infile:
    lines = infile.read().strip().split("\n")

tprods = {}

for line in lines:
    for pair in line.split(" "):
        token, tag = pair.split("/")
        token = token.lower()
        ttoken = token.title()
        if tag not in tprods:
            tprods[tag] = set()
        tprods[tag].update(set((token, ttoken)))

output = "\n".join(["{0} -> {1}".format(preterminal, " | ".join("'{0}'".format(terminal) for terminal in sorted(list(tprods[preterminal])))) for preterminal in tprods])

try:
    outpath = sys.argv[2]
    with open(outpath, "w") as outfile:
        outfile.write(output)
except IndexError:
    sys.stdout.write(output + "\n")
